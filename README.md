# refcapsule

Safely send references to other threads in rust

A way to create a scope in which you can send references across a channel

## Example:


```rust
use std::thread;
use std::time::Duration;
use std::sync::mpsc::channel;
use refcapsule::{Capsule, with_encapsulated};

let (sender, receiver) = channel::<Capsule<u32>>();

// receiver of references

thread::spawn(move || {
    {
        let r = receiver.recv().unwrap();
        thread::sleep(Duration::from_millis(100));
        assert_eq!(*r, 4);
    }
    {
        let r = receiver.recv().unwrap();
        thread::sleep(Duration::from_millis(100));
        assert_eq!(*r, 12);
    }
});

let x: u32 = 4;
let s1 = sender.clone();
with_encapsulated(&x, move |x| s1.send(x).unwrap());

with_encapsulated(&12, move |cap| sender.send(cap).unwrap());
```
